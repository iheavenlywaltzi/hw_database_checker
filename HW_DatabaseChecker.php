<?php
/*
 *
 * coded by HW
 *
 */


class HW_DatabaseChecker
{
    private $_DBC = null; // Бд селектор ( оно же БД коннект )
    private int $_LEN_INDEX = 20; // Минимальный размер длины индекса. ( по умолчанию  не для всех типов )
    private $historyQuery = [];

    public function getHistory()
    {
        return $this->historyQuery;
    }

    function __construct($db_con = false)
    {
        $this->_DBC = $db_con;
    }

    // установит коннект к БД
    public function setConnectDB($db_con = false)
    {
        $this->_DBC = $db_con;
    }

    // Проверит сущестует ли Таблица, создаст если нету, проверит её стуктуру, создаст по шаблону пришедшего массива
    public function initTable($infData)
    {
        if (is_array($infData)) {
            foreach ($infData as $tableName => $tableData) {
                $columns = $tableData['columns'] ?? [];
                $indexs = $tableData['indexs'] ?? [];
                $create_sql = $tableData['create_sql'] ?? [];

                if ($this->checkTable($tableName)) {
                    $this->initColum($tableName, $columns);
                    ($indexs) ? $this->initIndex($tableName, $tableData) : '';
                } else {
                    $is_create_rable = $this->addTable($tableName, $columns);
                    ($indexs && $is_create_rable) ? $this->initIndex($tableName, $tableData) : '';
					if($create_sql){
						foreach ($create_sql as $dsql){
							$this->query("$dsql");
						}
					}
                }
            }
        }
    }

    // Проверит существуют ли колонки в указанной таблице, по шаблонному масиву
    public function initColum($tableName, $dbData)
    {
        if ($tableName && is_array($dbData)) {
            $scheme = $this->getSchemaTable($tableName);

            $deleteCol = array_diff_key($scheme, $dbData);
            $addCol = array_diff_key($dbData, $scheme);

            // Добавим если есть что
            foreach ($addCol as $key_newCol => $newCol) {
                $strSqlColum = $this->createSqlColum($newCol);
                $queryText = "ALTER TABLE $tableName ADD COLUMN $key_newCol " . $strSqlColum;
                $this->query($queryText);
            }

            // Удалим если есть что
            foreach ($deleteCol as $key_delCol => $delCol) {
                $queryText = "ALTER TABLE $tableName DROP COLUMN $key_delCol";
                $this->query($queryText);
            }
        }
    }

    // Проверит существуют ли индексы в указанной таблице, по шаблонному масиву
    public function initIndex($tableName, $tableData)
    {
        if ($tableName && is_array($tableData)) {

            $indedxsTable = $this->getIndexsTable($tableName);
            $idexData = $tableData['indexs'] ?? [];
            $colData = $tableData['columns'] ?? [];

            // Проверим есть ли уникальные элементы чтобы, слуйчайно не удалить ( обычно это ID );
            foreach ($colData as $key_colData => $val_сolData) {
                if ($val_сolData['unique']) {
                    $idexData[$key_colData] = [
                        'type' => 'UNIQUE',
                        'cols' => [['name' => $key_colData]],
                    ];
                }
            }

            $deleteCol = array_diff_key($indedxsTable, $idexData);
            $addCol = array_diff_key($idexData, $indedxsTable);

            // Добавим если есть что
            foreach ($addCol as $key_addIndex => $newIndex) {
                $cols = $newIndex['cols'] ?? [];
                $type = strtolower($newIndex['type'] ?? 'INDEX'); // FULLTEXT

                if ($type != 'index') {
                    $type = "$type INDEX";
                }

                $addCol = [];
                foreach ($cols as $key_cols => $val_cols) {
                    if ($colData[$val_cols['name']]) {
                        $val_cols['type'] = strtolower($colData[$val_cols['name']]['type']);
                        $len = $val_cols['len'] ?? false; // $len

                        if ($len && $val_cols['type'] != 'int') { // Если указали длину индекста
                            $addCol[] = $val_cols['name'] . "($len)";
                        } else if ($type == 'index' && $val_cols['type'] != 'int') { // Если НЕ указали длину индекста,сам индекс = index, тип ячейки не int
                            $addCol[] = $val_cols['name'] . "(" . $this->_LEN_INDEX . ")";
                        } else {
                            $addCol[] = $val_cols['name'];
                        }
                    }
                }
                $addColStr = implode(",", $addCol);
                $queryText = '';

                if ($addColStr) {
                    $queryText = "CREATE $type $key_addIndex ON $tableName ($addColStr)";
                    $this->query($queryText);
                }
            }

            // Удалим если есть что
            foreach ($deleteCol as $key_delIndex => $delIndex) {
                $queryText = "ALTER TABLE $tableName DROP INDEX $key_delIndex;";
                $this->query($queryText);
            }
        }
    }

    // Добовляет таблицу, по шаблонному масиву
    public function addTable($tableName, $dbData)
    {
        if ($tableName && is_array($dbData)) {
            $bottomKeyUnique = [];
            $bottomKeyUniqueText = '';
            $queryText = "CREATE TABLE $tableName (";

            foreach ($dbData as $key_newCol => $newCol) {
                $type = strtolower($newCol['type'] ?? 'int');
                $len = ($newCol['len']) ? "({$newCol['len']})" : "";

                if ($type == 'varchar' && !$len) {
                    $len = '(255)';
                }
                $strSqlColum = $this->createSqlColum($newCol);

                $queryText .= "$key_newCol $strSqlColum, ";
                if ($newCol['unique']) {
                    $bottomKeyUnique[] = $key_newCol;
                }
            }
            $queryText = mb_substr($queryText, 0, -2);

            if ($bottomKeyUnique) {
                $queryText .= ', ';
                foreach ($bottomKeyUnique as $key_unique => $unique) {
                    $bottomKeyUniqueText .= "UNIQUE ($unique), ";
                }
                $queryText .= mb_substr($bottomKeyUniqueText, 0, -2);
            }
            $queryText .= "); ";

            return $this->query($queryText);
        }
    }

    //Просто проверит существует ли БД и всё
    public function checkTable($dbName)
    {
        $response = false;
        if ($quer = $this->query("CHECK TABLE $dbName")) {
            $status = $this->getOneRow($quer)['Msg_type'];
            if ($status == 'status') {
                $response = true;
            }
        }
        return $response;
    }



    /* PRIVATE */

    // Вернёт схему таблици
    private function getSchemaTable($table)
    {
        $quer = $this->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='$table'");
        $scheme = $this->getAllKey($quer, 'COLUMN_NAME');
        return $scheme;
    }

    // Вернёт индексы таблици
    private function getIndexsTable($table)
    {
        $quer = $this->query("SHOW INDEXES FROM $table");
        $indexs = $this->getAllKey($quer, 'Key_name');
        return $indexs;
    }

    // Просто запрос к БД
    private function query($text)
    {
        $this->historyQuery[] = $text;
        return mysqli_query($this->_DBC, $text);
    }

    // Освободит памть запроса
    private function free($result)
    {
        mysqli_free_result($result);
    }

    // Вернёт всего 1 строчку ( 1ю ) из запроса
    private function getOneRow($result, $mode = MYSQLI_ASSOC)
    {
        return mysqli_fetch_array($result, $mode);
    }

    // Создаст SQL строку для запроса по колонке
    private function createSqlColum($dataColum)
    {
        $strSql = '';

        $type = strtolower($dataColum['type'] ?? 'int');
        $len = ($dataColum['len']) ? "({$dataColum['len']})" : "";
        $sql = $dataColum['sql'] ?? "";

        if ($type == 'varchar' && !$len) {
            $len = '(255)';
        }

        $strSql = "$type$len $sql";

        return $strSql;
    }

    // Вернт все строки из запроса, где key если есть, будет ключём каждого элемента
    private function getAllKey($quer, $key = false)
    {
        $response = array();
        while ($row = $this->getOneRow($quer)) {
            ($key && $row[$key]) ? $response[$row[$key]] = $row : $response[] = $row;
        }
        $this->free($quer);
        return $response;
    }
} ?>
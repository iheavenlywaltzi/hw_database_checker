<?php return [
	'tables' => [
		'test_db_new_fcn' => [
			'columns' => [
				'id' => [
					'sql' => 'NOT NULL AUTO_INCREMENT',  // дополнительный запрос
					'type' => 'int',  // Обязательное поле Тип колонки
					'len' => '11',  // Длина колонки
					'unique' => true, // Влияет на ИНДЕКСЫ
				],
				'name' => ['type' => 'text', 'len' => '11'],
				'id_parent' => ['type' => 'int', 'len' => '11'],
				'description' => ['type' => 'varchar'],
				'time' => ['type' => 'int', 'len' => '20'],
			],
			'create_sql'=>[ // Выполнит эти СКЛ запросы толко после СОЗДАНИЕ НОВОЙ БД, если бд существует они НЕВЫПОЛНЯТСЯ!
				"INSERT INTO `test_db_new_fcn` (`name`, `description`) VALUES ( 'vasa1', 'vsdsdvs')",
				"INSERT INTO `test_db_new_fcn` (`name`, `description`) VALUES ( 'vasa', '13')",
				"INSERT INTO `test_db_new_fcn` (`name`, `description`) VALUES ( 'petia', 'dvew')",
				"INSERT INTO `test_db_new_fcn` (`name`, `description`) VALUES ( 'lola', 'vsprite')"
			],
			'indexs' => [
				'test_i' => [
					'type' => 'INDEX', // default = INDEX  , FULLTEXT ,
					'cols' => [
						['name' => 'id', 'len' => '10'],
						['name' => 'description','len' => '10']
					]
				],
				'test_ii' => [
					'type' => 'INDEX',
					'cols' => [
						['name' => 'id'],
						['name' => 'name']
					]
				],
				'test_iii' => [
					'type' => 'FULLTEXT',
					'cols' => [
						['name' => 'name'],
						['name' => 'description', 'len' => '40']
					]
				]
			]
		],
		'test_db_new_fcn_2' => [
			'columns' => [
				'id' => [
					'sql' => 'NOT NULL AUTO_INCREMENT', 
					'type' => 'int', 
					'len' => '11', 
					'unique' => true 
				],
				'name' => ['type' => 'text'],
				'id_parent' => ['type' => 'int', 'len' => '11'],
				'description' => ['type' => 'varchar', 'len' => '255'],
				'time' => ['type' => 'int', 'len' => '11'],
			],
			'indexs' => [
				'test_i' => [
					'type' => 'INDEX',
					'cols' => [
						['name' => 'id'],
						['name' => 'name'],
						['name' => 'description', 'len' => '40']
					]
				],
			]
		]
	]
]; ?>
